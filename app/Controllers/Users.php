<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Files\File;
use App\Models\UserModel;
use League\Csv\Reader;

class Users extends Controller
{
    protected $session = null; // use protected instead as recommended
    public function list()
    {
        $model = new UserModel();

        // $data['users'] = $model->orderBy('id', 'DESC')->findAll();
        $query = $model->query('select * from users');
        $data['users'] = $query->getResultArray();
        return view('users', $data);
    }

    public function create()
    {
        return view('create-user');
    }

    public function store()
    {

        helper(['form', 'url']);

        $model = new UserModel();
        $name =  $this->request->getVar('name');
        $email = $this->request->getVar('email');
        $sql = "INSERT INTO users (name, email) values (?, ?)";
        $save = $model->query($sql, array($name, $email));

        return redirect()->to(base_url('users'));
    }

    public function csvImport()
    {
        helper(['form', 'url']);
        $model = new UserModel();
        $validationRule = [
            'userfile' => [
                'rules' => 'ext_in[userfile,csv]'
            ],
        ];

        if (!$this->validate($validationRule)) {
            $data = ['errors' => $this->validator->getErrors()];
            foreach ($data as $item) {
                $msg =  $item['userfile'];
            }
            return redirect()->to(base_url('users'))->with('message', $msg);
        } else {
            $file_path =  $this->request->getFile('userfile');
            $csv = Reader::createFromPath($file_path)->setHeaderOffset(0);
            foreach ($csv as $record) {
                $sql = "INSERT INTO users (name, email) values (?, ?)";
                $save = $model->query($sql, array($record['name'], $record['email']));
            }
            $msg = "Success!! CSV file imported into database successfully!";
        }
        return redirect()->to(base_url('users'))->with('message', $msg);
    }
}
