<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Codeigniter 4 Example </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>

    <div class="container mt-5">
        <h1>User list view</h1>
        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo site_url('/create') ?>" class="btn btn-success mb-2">Create</a>
            </div>
            <div class="col-md-8">
                <?php if (session('message')) : ?>
                    <div class="alert alert-info">
                        <?= session('message') ?>
                    </div>
                <?php endif ?>
                <form action="<?php echo base_url('csv-import'); ?>" name="csv_import" id="csv_import" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="csv_file">Upload CSV File</label>
                        <input type="file" name="userfile" class="form-control" id="userfile" required>


                    </div>
                    <div class="form-group">
                        <button type="submit" id="send_form" class="btn btn-success">Import</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mt-3">
            <table class="table table-bordered" id="users">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($users) : ?>
                        <?php foreach ($users as $user) : ?>
                            <tr>
                                <td><?php echo $user['id']; ?></td>
                                <td><?php echo $user['name']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</body>

</html>